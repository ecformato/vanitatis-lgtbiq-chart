/*
* Comenzar el proceso de análisis y visualización cuando el documento esté listo
*/
let buttons = document.getElementsByClassName('btn');
let tooltip = document.getElementById('ecTooltip');
let currentType = 'chartHomosexualidad';
let nestedData = [];

if (document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll)) {
    initData();
} else {
    document.addEventListener("DOMContentLoaded", initData);
}

for(let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener('click', function(e) {
        let btn = e.target;
        drawItems(btn);
    });
}

function initData() {
    d3.csv('https://docs.google.com/spreadsheets/d/e/2PACX-1vRxoMocvCTdqlIg5Zfwv4Metn0AXwbA803lMPooOjWk9KeWGx2XDKE8BVGYW5BLSF_0LEasU10FCRFb/pub?gid=0&single=true&output=csv', function(error, data) {
        if (error) throw error;
        nestedData = d3.nest().key(function(d){return d['Región'];}).entries(data);
                
        drawType(currentType);
    });
}

/* Helpers > Pintado de datos */
function drawItems(btn) {    
    drawButton(btn);
    changeLegend(btn.id);
    drawType(btn.id);
}

function drawType(type) {
    currentType = type;
    //Lógica de borrado
    let chartsViz = document.getElementsByClassName('chart--viz');

    for(let i = 0; i < chartsViz.length; i++) {
        let item = chartsViz[i];

        while(item.hasChildNodes()) {
            item.removeChild(item.firstChild);
        }
    }

    //Disposición de elementos
    for(let i = 0; i < nestedData.length; i++) {
        let regionData = nestedData[i].values;

        //Ordenación
        regionData = regionData.sort(compare);
        let regionBlock = '';

        if(nestedData[i].key == 'África') {
            regionBlock = document.getElementsByClassName('chart--viz__africa')[0];
        } else if (nestedData[i].key == 'América') {
            regionBlock = document.getElementsByClassName('chart--viz__america')[0];
        } else if (nestedData[i].key == 'Asia') {
            regionBlock = document.getElementsByClassName('chart--viz__asia')[0];
        } else if (nestedData[i].key == 'Europa') {
            regionBlock = document.getElementsByClassName('chart--viz__europa')[0];
        } else {
            regionBlock = document.getElementsByClassName('chart--viz__oceania')[0];
        }

        for (let i = 0; i < regionData.length; i++) {
            let country = regionData[i];

            let countryParentBlock = document.createElement('div');
            countryParentBlock.classList.add('chart--outercircle');

            let countryBlock = document.createElement('div');
            countryBlock.classList.add('chart--innercircle');
            countryBlock.setAttribute('data-country', country['País']);
            countryBlock.setAttribute('data-odio', country['Crimen de odio']);
            countryBlock.setAttribute('data-adopcion', country['Adopción']);
            countryBlock.setAttribute('data-homosexualidad', country['La homosexualidad es legal']);

            countryParentBlock.appendChild(countryBlock);
            regionBlock.appendChild(countryParentBlock);

            countryBlock.addEventListener('mouseover', function(e) {
                drawTooltip(e.target);
            });
            countryBlock.addEventListener('mouseout', function(e) {
                deleteTooltip();
            });
        }
    }

    //Pintado
    let attribute = '';
    if(type == 'chartHomosexualidad') {
        attribute = 'data-homosexualidad';
    } else if (type == 'chartAdopcion') {
        attribute = 'data-adopcion';
    } else if (type == 'chartOdio') {
        attribute = 'data-odio';
    }

    let circles = document.getElementsByClassName('chart--innercircle');

    for(let i = 0; i < circles.length; i++) {
        let item = circles[i];
        let data = item.getAttribute(attribute);

        //Reordenación

        //Pintado
        if(data != 'NO') {
            item.classList.add('chart--innercircle--active');
        } else {
            item.classList.remove('chart--innercircle--active');
        }
    }
}

function drawButton(btn) {
    for(let i = 0; i < buttons.length; i++) {
        buttons[i].classList.remove('btn--active');
    }
    btn.classList.add('btn--active');
}

function changeLegend(type) {

    if(type == 'chartOdio') {
        document.getElementsByClassName('legend--odio')[0].classList.remove('hidden');
        document.getElementsByClassName('legend--lgtbiq')[0].classList.add('hidden');
    } else {
        document.getElementsByClassName('legend--odio')[0].classList.add('hidden');
        document.getElementsByClassName('legend--lgtbiq')[0].classList.remove('hidden');
    }
}

/* Helpers > Tooltip */
function drawTooltip(country) {
    let tooltipHtml = '';

    tooltipHtml += '<p class="popup-title">' + country.getAttribute('data-country') + '</p>';

    if(currentType == 'chartHomosexualidad') {
        let texto = country.getAttribute('data-homosexualidad');
        texto = texto == 'YES' ? 'Sí' : texto.charAt(0).toUpperCase() + texto.slice(1).toLowerCase();
        tooltipHtml += '<p class="popup-text">¿Está permitida la homosexualidad? <b>' + texto  +'</b></p>';
    } else if (currentType == 'chartOdio') {
        let texto = country.getAttribute('data-odio');
        texto = texto == 'YES' ? 'Sí' : texto == 'LIMITED' ? 'Limitado' : 'No';
        tooltipHtml += '<p class="popup-text">¿Existe el delito de odio? <b>' + texto  +'</b></p>';
    } else if (currentType == 'chartAdopcion') {
        let texto = country.getAttribute('data-adopcion');
        texto = texto == 'YES' ? 'Sí' : texto == 'LIMITED' ? 'Limitado' : 'No';
        tooltipHtml += '<p class="popup-text">¿Es posible adoptar? <b>' + texto  +'</b></p>';
    }

    //Texto
    tooltip.innerHTML = tooltipHtml;
    //Opacidad
    tooltip.style.opacity = 0.9;
    //Posición
    let x = event.clientX || window.event.clientX;
    let y = event.clientY || window.event.clientY;
    let left = window.innerWidth / 2 > x ? 'right' : 'left';
    let horizontalPos = left == 'left' ? - 195 : 25;

    tooltip.style.top = y - 50 + 'px';
    tooltip.style.left = x + horizontalPos + 'px';
}

function deleteTooltip() {
    tooltip.style.opacity = 0;
}

//////
// Helpers
//////
function compare(a) {
    let data = '';

    if (currentType == 'chartHomosexualidad') {
        data = 'La homosexualidad es legal';
    } else if (currentType == 'chartOdio') {
        data = 'Crimen de odio';
    } else if (currentType == 'chartAdopcion') {
        data = 'Adopción';
    }

    if (a[data] == 'YES' || a[data] == 'DE FACTO' || a[data] == 'LIMITED'){
        return -1;
    } else {
        return 1;
    }
}